import os
import datetime
import subprocess
import psycopg2
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE
from email import encoders
from tkinter import *

from flask import Flask, render_template, request
import subprocess

app = Flask(__name__)

# Define the username and password for the Rancher K3s Desktop admin portal
username = "admin"
password = "password"

@app.route('/')
def index():
    return render_template('login.html')

@app.route('/dashboard', methods=['POST'])
def dashboard():
    if request.form['username'] == username and request.form['password'] == password:
        # Run a command to get the status of the K3s cluster
        k3s_status = subprocess.check_output(['sudo', 'k3s', 'kubectl', 'get', 'nodes'], stderr=subprocess.STDOUT, universal_newlines=True)
        return render_template('dashboard.html', k3s_status=k3s_status)
    else:
        return "Incorrect username or password"

if __name__ == '__main__':
    app.run(debug=True)
    
    
# Define database connection parameters
dbname = "caci-rancher0"
user = "postgresql user"
password = "my_password"
host = "localhost"
port = "5432"

# Define the log table name
log_table = "environment_changes"

# Define email parameters
smtp_server = "smtp.gmail.com"
smtp_port = 587
smtp_username = "amj22240@email.vccs.edu"
smtp_password = "!4Red3Skins95!*"
recipient_email = "{austin.jones@caci.com"}
username = "exampleuser"

# Check if the sudoers file exists
if not os.path.isfile('/etc/sudoers'):
    print("sudoers file not found")
    exit()

# Check if the user already has sudo privileges
output = subprocess.check_output(['sudo', '-l', '-U', username], stderr=subprocess.STDOUT, universal_newlines=True)
if "not allowed to run sudo" not in output:
    print("User " + username + " already has sudo privileges")
    exit()

# Add the user to the sudoers file
with open('/etc/sudoers', 'a') as f:
    f.write(username + ' ALL=(ALL) ALL\n')

print("User " + username + " granted sudo privileges")
# Function to log environment changes to a PostgreSQL database
def log_environment_changes(change):
    # Create a connection to the database
    conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port)
    # Create a cursor object
    cur = conn.cursor()
    # Construct the SQL statement
    sql = f"INSERT INTO {log_table} (change_timestamp, environment_change) VALUES (%s, %s)"
    # Get the current timestamp
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    # Execute the SQL statement with the timestamp and change
    cur.execute(sql, (timestamp, change))
    # Commit the transaction
    conn.commit()
    # Close the cursor and connection
    cur.close()
    conn.close()

# Define Install
def install(): 
    subprocess.run(['yum update', 'yum install sudo', ])
    os.system("visudo")



# Define a function to check if an update is needed
def check_update():
    # Check if update is needed by comparing the cache age to the defined threshold (in seconds)
    threshold = 3600  # 1 hour
    cache_age = datetime.time.time() - os.stat('/var/cache/apt/pkgcache.bin').st_mtime
    if cache_age > threshold:
        return True
    else:
        return False

# Define a function to run sudo apt update
def run_update():
    subprocess.run(['sudo', 'apt', 'update'])

# Check if an update is needed and run it if necessary
if check_update():
    run_update()
# Example environment change - adding Red Hat Cloud CLI
os.system("sudo subscription-manager repos --enable rhel-7-server-extras-rpms")
os.system("sudo yum install rh-python36")
os.system("scl enable rh-python36 bash")
log_environment_changes("Added Red Hat Cloud CLI to the terminal environment")

# Example environment change - installing Rancher Desktop
os.system("sudo snap install rancher-desktop --classic")
log_environment_changes("Installed Rancher Desktop using snap")

# Example environment change - updating system packages
os.system("sudo yum update")
log_environment_changes("Updated system packages")

# Example environment change - activating Python virtual environment
os.system("source myvenv/bin/activate")
log_environment_changes("Activated Python virtual environment")

# Send certificates and keys to a specific email address
subject = "Blockchain node certificates and keys"
body = "Please find attached the certificates and keys for the blockchain node."
sender_email = smtp_username
files = ["node_cert.pem", "node_key.pem"]
msg = MIMEMultipart()
msg['From'] = sender_email
msg['To'] = recipient_email
msg['Subject'] = subject
msg.attach(MIMEText(body))
for file in files:
    with open(file, "rb") as f:
        part = MIMEBase('application', 'octet-stream')
        part.set_payload(f.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', f'attachment; filename="{file}"')
        msg.attach(part)
text = msg.as_string()
server = smtplib.SMTP(smtp_server, smtp_port)
server.starttls()
server.login(smtp_username, smtp_password)
server.sendmail(sender_email, recipient_email, text)
server.quit()

# User interface for Rancher Desktop on CentOS 7
def launch():
    