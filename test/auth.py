from flask import Flask, request, jsonify

app = Flask(__name__)

# hard-coded users for simplicity
users = {
    "user1": "password1",
    "user2": "password2",
}

# access token generation function
def generate_access_token(username):
    # add some logic to generate an access token here
    # for simplicity, we'll just return the username
    return username

# endpoint for user authentication and access token generation
@app.route("/auth", methods=["POST"])
def auth():
    data = request.json
    username = data.get("username")
    password = data.get("password")

    if username in users and users[username] == password:
        access_token = generate_access_token(username)
        return jsonify({"access_token": access_token})
    else:
        return jsonify({"message": "Invalid username or password"}), 401

# endpoint for environment change logging
@app.route("/environment", methods=["POST"])
def log_environment_changes():
    # add some logic to log environment changes to the database here
    # for simplicity, we'll just return a success message
    return jsonify({"message": "Environment changes logged successfully"})

# middleware to check for access token
@app.before_request
def check_auth():
    if request.path == "/auth":
        return

    access_token = request.headers.get("Authorization")
    if not access_token:
        return jsonify({"message": "Access token missing"}), 401

    # add some logic to validate the access token here
    # for simplicity, we'll just check if it matches the username
    username = access_token
    if not username in users:
        return jsonify({"message": "Invalid access token"}), 401

    # if access token is valid, allow request to proceed
    request.username = username

    if request.path == "/environment":
        return jsonify({"message": "Create an Environment Change Logging API"})

if __name__ == "__main__":
    app.run()