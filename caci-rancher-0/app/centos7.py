import os
import subprocess
import docker
import os

# Set up the Docker client
client = docker.from_env()

# Build the Docker image for the application
docker_build_cmd = "docker build -t my-k3s-app ." # Needs to return the result of the name image built
# Build the Docker image and get its name
image, logs = client.images.build(path="path/to/dockerfile", tag="my-docker-image")
image_name = image.tags[0]

# Print the name of the Docker image
print("Docker image name: {}".format(image_name)) 
subprocess.call(docker_build_cmd, shell=True)

# Pull the Docker image from a registry
image_name = "my-docker-image"
image_tag = "latest"
image = client.images.pull(image_name, tag=image_tag)

# Create a Docker container from the image
container_name = "my-docker-container"
container_port = 8080
container = client.containers.run(image, name=container_name, ports={container_port: container_port}, detach=True)

# Print the container ID and IP address
print("Container ID: {}".format(container.id))
print("Container IP: {}".format(container.attrs['NetworkSettings']['IPAddress']))

# Perform maintenance on the container (e.g., stop, start, restart, remove)
container.stop()
container.start()
container.restart()
container.remove()

# Start the K3s cluster
k3s_start_cmd = "curl -sfL https://get.k3s.io | INSTALL_K3S_VERSION=v1.20.4+k3s1 sh -"
subprocess.call(k3s_start_cmd, shell=True)

# Deploy the Docker image to the K3s cluster
k3s_deploy_cmd = "kubectl create deployment my-k3s-app --image=my-k3s-app"
subprocess.call(k3s_deploy_cmd, shell=True)

# Expose the deployment as a service
k3s_expose_cmd = "kubectl expose deployment my-k3s-app --port=80 --type=LoadBalancer"
subprocess.call(k3s_expose_cmd, shell=True)

# Get the external IP address of the service
get_ip_cmd = "kubectl get service my-k3s-app -o=jsonpath='{.status.loadBalancer.ingress[0].ip}'"
external_ip = subprocess.check_output(get_ip_cmd, shell=True).decode().strip()

# Print the external IP address for the user
print("Your application is running at: http://{}".format(external_ip))
