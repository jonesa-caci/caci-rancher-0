#start up script for running an environment. checks for accurate and working installations of the minimum requirements for a rancher build
# Exports a path with minimum environment requirements 
export PATH="/usr/bin:$PATH"
export PATH="/usr/local/bin:$PATH"
export USER="$USER"
#install java
sudo tar zxvf jre-8u73-linux-x64.tar.gz

# Check for Docker
type "docker"
docker

#install postgresql
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install postgresql


sudo mkdir /var/lib/pgadmin
sudo mkdir /var/log/pgadmin
sudo chown $USER /var/lib/pgadmin
sudo chown $USER /var/log/pgadmin
python3 -m venv pgadmin4
source pgadmin4/bin/activate
#(pgadmin4) $ pip install pgadmin4
#...
#(pgadmin4) $ pgadmin4
#NOTE: Configuring authentication for SERVER mode.

#Enter the email address and password to use for the initial pgAdmin user account:

#Email address: user@domain.com
#Password: 
#Retype password:

# from the terminal in the project folder
mkdir templates static
touch app.py
cd templates
touch index.html
tree (optional: only works if tree is installed on OSX)



#Check for Java
type "java"

# Set Jfrog
wget -O jfrog-compose-installer.tar.gz "https://releases.jfrog.io/artifactory/jfrog-prox/org/artifactory/pro/docker/jfrog-platform-trial-prox/[RELEASE]/jfrog-platform-trial-prox-[RELEASE]-compose.tar.gz"
tar -xvzf jfrog-compose-installer.tar.gz
cd jfrog-platform-trial-pro*
sudo ./config.sh
sudo docker-compose -p trial-pro-rabbitmq -f docker-compose-rabbitmq.yaml up -d
sudo docker-compose -p trial-pro up -d
cd /mnt/c/Users/Aus/K3S/caci-rancher-0/jfrog-platform-trial-prox-7.55.9-compose
docker-compose -p trial-prox-rabbitmq -f docker-compose-rabbitmq.yaml up -d
docker-compose -p trial-prox-postgres -f docker-compose-postgres.yaml up -d
docker-compose -p trial-prox up -d

#check helm REPO_KEY = cacicorenet-0 (file://DEVICE1/cacicorenet-0)
type "helm"
helm
# Build Registry Container
helm repo add twuni https://helm.twun.io
helm install my-docker-registry twuni/docker-registry --version 2.2.2

export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
kubectl create namespace cattle-system

#helm repo add cacicorenet-0 (file://DEVICE1/cacicorenet-0) http://<ARTIFACTORY_HOST>:<ARTIFACTORY_PORT>/artifactory/cacicorenet-0 (file://DEVICE1/cacicorenet-0) --username <USERNAME> --password <PASSWORD>
#helm repo update




#Check Kube
#type "kubectl"
#kubectl
# Install K3s if needed




