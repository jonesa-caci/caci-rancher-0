import virtualbox
vbox = virtualbox.VirtualBox()

# Connect to the VirtualBox API
vbox = vboxapi.VirtualBoxManager().getVirtualBox()

# Create a new virtual machine
vm_name = "CentOS 7 Server"
vm = vbox.createMachine("", vm_name, [], "Linux", "")
vm.memorySize = 2048
vm.description = "CentOS 7 Server"

# Configure the virtual machine settings
ostype = "RedHat_64"
cpus = 2
boot_order = ["dvd", "disk"]
vm_settings = vm.getSettings()
vm_settings.setMemorySize(vm.memorySize)
vm_settings.setCPUCount(cpus)
vm_settings.setBootOrder(boot_order)
vm_settings.setChipsetType("PIIX3")
vm_settings.setPointingHidType("PS2Mouse")
vm_settings.setKeyboardHidType("PS2Keyboard")

# Create a new virtual hard drive
vdi_filename = "centos7_server.vdi"
vdi_path = "/path/to/virtual/hard/drive/folder/" + vdi_filename
vdi_size_mb = 10240
vdi_size_bytes = vdi_size_mb * 1024 * 1024
vdi = vbox.createHardDisk("VDI", vdi_path)
vdi.formatSize(vdi_size_bytes)

# Attach the virtual hard drive to the virtual machine
storage_ctl = vm_settings.getStorageControllers()[0]
storage_ctl_name = storage_ctl.getName()
storage_ctl.setAttachmentType("IDE", "Primary")
storage_ctl_port = 0
storage_ctl_device = 0
vm.attachDevice(storage_ctl_name, storage_ctl_port, storage_ctl_device, vbox.constants.DeviceType_HardDisk, vdi)

# Attach the CentOS 7 ISO file to the virtual machine
iso_path = "/path/to/centos7_server.iso"
iso = vbox.openMedium(iso_path, vbox.constants.DeviceType_DVD, vbox.constants.AccessMode_ReadOnly, False)
vm.attachDevice(storage_ctl_name, storage_ctl_port, 1, vbox.constants.DeviceType_DVD, iso)

# Create a new session and launch the virtual machine
session = vbox.getSessionObject(vbox)
progress = vm.launchVMProcess(session, "gui", "")
progress.waitForCompletion(-1)

# Wait for the virtual machine to shut down
while vm.getState() != vbox.constants.MachineState_Aborted:
    time.sleep(1)

# Detach the virtual hard drive and delete it
vm.detachDevice(storage_ctl_name, storage_ctl_port, storage_ctl_device)
vdi.deleteStorage()

# Remove the virtual machine
vbox.unregisterMachine(vm)
