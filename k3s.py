import yaml

team = []

data_server = {
    'servername':'Rancher',
    'username':'admin',
    'Location':'us-east-1',
    'Permissions': {'Docker':'Read/Write',\
    'kubectl':'Read/Write', 'Helm':'Read/Write',\
    'Rancher':'Read/Write'},
    'Languages': {'Markup':['HTML', 'CSS', '.md'], 'Programming'\
    :['Python', 'JavaScript','Golang', 'Java', 'C', '.NET', 'JSON']}
}

data_user = {
    'Name':'admin',
    'Position':'DevOps Team',
    'Location':'us-east-1',
    'Permissions': {'Docker':'Read/Write',\
    'kubectl':'Read/Write', 'Helm':'Read/Write',\
    'Rancher':'Read/Write'},
    'Languages': {'Markup':['HTML', 'CSS', '.md'], 'Programming'\
    :['Python', 'JavaScript','Golang', 'Java', 'C', '.NET', 'JSON']}
}

yaml_output = yaml.dump(data, sort_keys=False) 

print(yaml_output)

# creates empty list of admin users
team = ['caci-rancher-0']

print("Adding server to environment: ")
print(team)
 

