import os
import subprocess
import platform
import requests
import subprocess
import tarfile
from flask import Flask, request
import requests
import datetime
import psycopg2

import os
import datetime
import psycopg2

import tkinter as tk
import subprocess
from flask import Flask, render_template, request
from multiprocessing import Process

app = Flask(__name__)

# Define the username and password for the Rancher K3s Desktop admin portal
username = "admin"
password = "password"

@app.route('/')
def index():
    return render_template('login.html')

@app.route('/dashboard', methods=['POST'])
def dashboard():
    if request.form['username'] == username and request.form['password'] == password:
        # Run a command to get the status of the K3s cluster
        k3s_status = subprocess.check_output(['sudo', 'k3s', 'kubectl', 'get', 'nodes'], stderr=subprocess.STDOUT, universal_newlines=True)
        return render_template('dashboard.html', k3s_status=k3s_status)
    else:
        return "Incorrect username or password"

def start_flask():
    app.run(port=5000)

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.label = tk.Label(self, text="Rancher K3s Desktop Admin Portal")
        self.label.pack()

        self.browser = tk.Frame(self)
        self.browser.pack()

        self.webview = tk.Canvas(self.browser, width=800, height=600)
        self.webview.pack()

        self.process = Process(target=start_flask)
        self.process.start()

        self.master.protocol("WM_DELETE_WINDOW", self.on_closing)

    def on_closing(self):
        self.process.terminate()
        self.master.destroy()

root = tk.Tk()
app = Application(master=root)
app.mainloop()

# Define database connection parameters
dbname = "caci-rancher0"
user = "postgresql_user"
password = "my_password"
host = "localhost"
port = "5432"

# Define the log table name
log_table = "environment_changes"

# Function to log environment changes to a PostgreSQL database
def log_environment_changes(change):
    # Create a connection to the database
    conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port)
    # Create a cursor object
    cur = conn.cursor()
    # Construct the SQL statement
    sql = f"INSERT INTO {log_table} (change_timestamp, environment_change) VALUES (%s, %s)"
    # Get the current timestamp
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    # Execute the SQL statement with the timestamp and change
    cur.execute(sql, (timestamp, change))
    # Commit the transaction
    conn.commit()
    # Close the cursor and connection
    cur.close()
    conn.close()

# Example environment change - adding Red Hat Cloud CLI
os.system("sudo subscription-manager repos --enable rhel-7-server-extras-rpms")
os.system("sudo yum install rh-python36")
os.system("scl enable rh-python36 bash")
log_environment_changes("Added Red Hat Cloud CLI to the terminal environment")

# Example environment change - installing Rancher Desktop
os.system("sudo snap install rancher-desktop --classic")
log_environment_changes("Installed Rancher Desktop using snap")

# Example environment change - updating system packages
os.system("sudo yum update")
log_environment_changes("Updated system packages")

# Example environment change - activating Python virtual environment
os.system("source myvenv/bin/activate")
log_environment_changes("Activated Python virtual environment")


app = Flask(__name__)
# Define the version of Helm to install
helm_version = "v3.7.0"

# Determine the operating system and architecture ????
system = platform.system().lower()
if system == "linux":
    arch = "amd64"
elif system == "darwin":
    arch = "darwin-amd64"
else:
    raise ValueError("Unsupported system: {}".format(system))

# Define the URL to download the Helm binary
helm_url = "https://get.helm.sh/helm-{}-{}-{}.tar.gz".format(helm_version, system, arch)

# Check if Helm is already installed
try:
    subprocess.run(["helm", "version"], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print("Helm is already installed.")
except subprocess.CalledProcessError:
    # Helm is not installed, so download and install it
    print("Helm is not installed, downloading and installing...")
    response = requests.get(helm_url)
    with open("helm.tar.gz", "wb") as f:
        f.write(response.content)
    with tarfile.open("helm.tar.gz", "r:gz") as tar:
        tar.extractall()
    os.remove("helm.tar.gz")
    os.rename("{}".format(os.path.join("{}", "helm".format(system))), "helm")
    subprocess.run(["chmod", "+x", "helm"])
    subprocess.run(["sudo", "mv", "helm", "/usr/local/bin/helm"])
    print("Helm has been installed.")

# Define Default space for a node to pull charts from 
# the name and URL of the Helm repository to add
# repo_name = "my-repo"
# repo_url = "https://my-repo.com/charts"

# Add the Helm repository to Kubernetes
# subprocess.run(["helm", "repo", "add", repo_name, repo_url])
###


# Define the name of the Rancher Desktop release and the namespace to deploy it in
release_name = "rancher-desktop"
namespace = "rancher-desktop"

# Define the Helm chart repository for Rancher Desktop
rancher_desktop_repo = "https://rancher.github.io/rancher-desktop-helm-chart"

# Add the Helm chart repository to Helm
subprocess.run(["helm", "repo", "add", "rancher-desktop", rancher_desktop_repo])

# Update the Helm chart repository to ensure we have the latest version of the Rancher Desktop chart
subprocess.run(["helm", "repo", "update"])

# Deploy Rancher Desktop using Helm
subprocess.run(["helm", "install", release_name, "rancher-desktop/rancher-desktop", "--namespace", namespace])

# Define the server endpoint for receiving messages
@app.route('/message', methods=['POST'])
def receive_message():
    message = request.get_json().get('message')
    if message:
        print("Received message: {}".format(message))
        return 'OK'
    else:
        return 'Error: no message provided'

# Define the client function for sending messages
def send_message(message):
    endpoint = os.environ.get('MESSAGE_ENDPOINT')
    if endpoint:
        response = requests.post(endpoint, json={'message': message})
        if response.status_code == 200:
            print("Message sent: {}".format(message))
        else:
            print("Error sending message: {}".format(response.text))
    else:
        print("Error: MESSAGE_ENDPOINT environment variable not set")

if __name__ == '__main__':
    # Start the server
    app.run(host='0.0.0.0', port=8080)


