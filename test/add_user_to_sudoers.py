import subprocess

# Replace "exampleuser" with the username of the user you want to grant sudo privileges to
username = "exampleuser"

# Check if the sudoers file exists
if not os.path.isfile('/etc/sudoers'):
    print("sudoers file not found")
    exit()

# Check if the user already has sudo privileges
output = subprocess.check_output(['sudo', '-l', '-U', username], stderr=subprocess.STDOUT, universal_newlines=True)
if "not allowed to run sudo" not in output:
    print("User " + username + " already has sudo privileges")
    exit()

# Add the user to the sudoers file
with open('/etc/sudoers', 'a') as f:
    f.write(username + ' ALL=(ALL) ALL\n')

print("User " + username + " granted sudo privileges")
