from flask import Flask, request, jsonify
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity
from werkzeug.security import generate_password_hash, check_password_hash
from psycopg2 import pool
import redis
import os
import subprocess
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE
from email import encoders

app = Flask(__name__)
app.config['SECRET_KEY'] = 'super-secret-key'

jwt = JWTManager(app)

# PostgreSQL connection pool
postgres_pool = pool.SimpleConnectionPool(
    minconn=1,
    maxconn=10,
    host=os.environ.get('POSTGRES_HOST', 'localhost'),
    port=os.environ.get('POSTGRES_PORT', '5432'),
    database=os.environ.get('POSTGRES_DB', 'mydatabase'),
    user=os.environ.get('POSTGRES_USER', 'myuser'),
    password=os.environ.get('POSTGRES_PASSWORD', 'mypassword')
)

# Elasticache connection
redis_host = os.environ.get('REDIS_HOST', 'localhost')
redis_port = os.environ.get('REDIS_PORT', '6379')
redis_password = os.environ.get('REDIS_PASSWORD', None)

if redis_password:
    redis_client = redis.Redis(host=redis_host, port=redis_port, password=redis_password)
else:
    redis_client = redis.Redis(host=redis_host, port=redis_port)


def get_db_connection():
    conn = postgres_pool.getconn()
    return conn


def return_db_connection(conn):
    postgres_pool.putconn(conn)


@app.route('/register', methods=['POST'])
def register():
    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    hashed_password = generate_password_hash(password, method='sha256')
    conn = get_db_connection()

    cursor = conn.cursor()
    cursor.execute('INSERT INTO users (username, password) VALUES (%s, %s)', (username, hashed_password))
    conn.commit()
    return_db_connection(conn)

    return jsonify({"msg": "User created successfully"}), 201


@app.route('/login', methods=['POST'])
def login():
    username = request.json.get('username', None)
    password = request.json.get('password', None)

    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    conn = get_db_connection()

    cursor = conn.cursor()
    cursor.execute('SELECT * FROM users WHERE username = %s', (username,))
    user = cursor.fetchone()
    return_db_connection(conn)

    if not user:
        return jsonify({"msg": "User not found"}), 401

    if check_password_hash(user[2], password):
        access_token = create_access_token(identity=username)
        return jsonify(access_token=access_token), 200
    else:
        return jsonify({"msg": "Wrong password"}), 401


@app.route('/environment_changes', methods=['POST'])
@jwt_required
def log_environment_changes():
    user = get_jwt_identity()
    environment_changes = request.json.get('environment_changes', None)

    if not environment_changes:
        return jsonify({"msg": "Missing environment_changes parameter"}), 
